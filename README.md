# Random Test Generator V2

## Description
A re-implementation of the original.

A Node is an abstract class that can generate text and can have optional parameters that are not included in that text.
A TextNode extends Node and generates text using whatever is in its name property.
A CompoundNode extends Node and as a group of parameters that it combines to generate its text. Parameters may be strings or other Nodes.
A SimpleNode extends Node and has a list of Nodes that it randomly selects from to generate its text.


## TODO

### Version 0.1.0
Minimum Viable Class Library

* ~~CompoundNode~~
    * ~~Unit Tests~~
    * ~~RemoveParameter()~~
    * ~~Recursive Limit~~
* ~~SimpleNode~~
    * ~~Sends OptionalParameters of selected TextNode~~
    * ~~Make Own OptionalParameters required of children and return the key if value is null || undefined.~~
    * ~~Need preserve capitalization option.~~
    * ~~Recursive Limit~~
* ~~Node~~
    * ~~Unit Tests~~
    * ~~Implement method for adding OptionalParameters.~~
    * ~~Implement method for removing OptionalParameters.~~
    * ~~Implement method for getting OptionalParameters.~~
* ~~TextNode~~
    * ~~Unit Tests~~

### Version 0.2.0
Persistent Storage

* Integrate MongoDB database
* Create store(): void;
    * Call store() on Nodes in CompoundNode.parameters
    * TextNodes are stringified.
    * Replace Simple or Complex Nodes with _id;
    * Simple Nodes can probably be stored as is.
* Import/Export JSON
    * On Export, Node.name is forced to be unique
    * On Import, Node.name must be unique.

### Version 0.3.0

* REST API

### Version 1.0.0

* Ionic Client
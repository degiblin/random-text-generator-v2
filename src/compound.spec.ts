import { } from "jasmine";
import { CompoundNode } from "./compound";
import { Node } from "./node";
import { TextNode } from "./text"

describe("A CompoundNode", () => {

    class TestNode extends Node {
        generateText(param?: any): string {
            if (param && Object.keys(this.optionalParameters).length > 0) {
                if (param) {
                    param[this.name] = this.optionalParameters;
                }
            }
            return this.name;
        }


    }

    let sut: CompoundNode;
    let testNode = new TextNode("TestNode1", 0);
    let testNode2 = new TextNode("TestNode2", 1);
    let testNode3 = new TextNode("TestNode3", 3);
    let testOptionalParamNode = new TestNode("Optional Test", 5);
    beforeEach(() => {
        console.log("\n[CompoundNode]")
        sut = new CompoundNode("System Under Test", 0);
        sut.parameters[testNode3.name] = testNode3;
        sut.parameters[testNode.name] = testNode;
        sut.parameters[testNode2.name] = testNode2;

        testOptionalParamNode.addOptionalParameters({ "TestParam": "TestValue" });
        sut.parameters[testOptionalParamNode.name] = testOptionalParamNode;
    });

    it('should return nodes in order', () => {
        let result = sut.generateText();
        expect(result).toContain(testNode.generateText() + " " + testNode2.generateText() + " " + testNode3.generateText() + " " + testOptionalParamNode.generateText());
    });

    it('should accept optional parameters for a given parameter', () => {
        console.log(testOptionalParamNode);

        sut.generateText();
        expect(sut.optionalParameters[testOptionalParamNode.name]).toEqual(testOptionalParamNode.optionalParameters);
    })

    it('createInstance() should create a new CompoundNode with string parameters and the supplied name', () => {
        let name = "ResultNode";
        let result = sut.createInstance(name);
        expect(result.name).toBe(name);
        expect(result.parameters[testNode.name]).toBe(testNode.generateText());
        expect(result.parameters[testNode2.name]).toBe(testNode2.generateText());
        expect(result.parameters[testNode3.name]).toBe(testNode3.generateText());
        expect(result.optionalParameters[testOptionalParamNode.name]).toEqual(testOptionalParamNode.optionalParameters);
    });

    it('addParameter() should add the Node to the correct slot', () => {
        let testParameter = new TestNode("Test Parameter");
        sut.addParameter(testParameter);
        expect(sut.parameters[testParameter.name]).toEqual(testParameter);
    })

    it('removeParameter() should remove the parameter with the given name', () => {
        let testParameter = new TestNode("Test Parameter");
        sut.addParameter(testParameter);
        expect(sut.parameters[testParameter.name]).toEqual(testParameter);
        sut.removeParameter(testParameter.name);
        expect(sut.parameters[testParameter.name]).toBeUndefined();
    })
    it('removeParameter() should remove the key with the given name', () => {
        let testParameter = new TestNode("Test Parameter");
        sut.addParameter(testParameter);
        expect(sut.parameters[testParameter.name]).toEqual(testParameter);
        expect(Object.keys(sut.parameters)).toContain(testParameter.name);
        sut.removeParameter(testParameter.name);
        expect(Object.keys(sut.parameters)).not.toContain(testParameter.name);
    })
    it('addParameters() should take an anonymous object and add any Node parameters', () => {
        let testParameter = new TestNode("Test Parameter");
        let paramObject = {}
        paramObject[testParameter.name] = testParameter;
        sut.addParameters(paramObject);
        expect(sut.parameters[testParameter.name]).toEqual(testParameter);
    });

    it('addParameters() should take an anonymous object and add any string parameters as TextNodes', () => {
        let testParameter = new TestNode("Test Parameter");
        let testString = "Hello World";
        let paramObject = {}
        paramObject[testParameter.name] = testParameter;
        paramObject[testString] = testString;
        sut.addParameters(paramObject);
        expect(sut.parameters[testParameter.name]).toEqual(testParameter);
        expect(sut.parameters[testString].name).toEqual(testString);
    });

/** Recursive Calls */

    class TempNode extends Node {
        list: Node[] = [];
        depth: number;
        limit: number;
        generateText(param?: any, recursiveDepth: number = 1, recursiveLimit: number = 1): string {
            this.depth = recursiveDepth;
            this.limit = recursiveLimit;
            let result = "";
            for (let i = 0; i < this.list.length; i++) {
                result += this.list[i].generateText();
            }
            return result;
        }
    }
    it('should have been called 3 times', () => {
        let result = new TempNode("Temp");
        spyOn(sut, "generateText");
        result.list.push(sut);
        result.list.push(sut);
        result.list.push(sut);
        result.generateText();
        expect(sut.generateText).toHaveBeenCalledTimes(3);
    });
    it('generateText() should accept recursiveDepth and recursiveLimit', () => {
        sut.generateText(null, 1, 1);
    });
    it('should increment recursiveDepth before passing it on to child nodes', () => {
        let result = new TempNode("Temp");
        sut.parameters = {"Temp": result};
        sut.generateText(null, 3, 5);
        expect(result.limit).toBe(5);
        expect(result.depth).toBe(4);
    });
})
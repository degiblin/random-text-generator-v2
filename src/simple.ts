import { Node } from "./node";
import { TextNode } from "./text";

export class SimpleNode extends Node {


    list: TextNode[] = [];

    selectRandom(list?: Node[]): Node {
        return (list) ? list[Math.floor(Math.random() * list.length)] : this.list[Math.floor(Math.random() * this.list.length)];
    }
    selectText(): TextNode {
        let textNodes = this.list.filter(value => {
            return value instanceof TextNode;
        });
        let node = (textNodes.length > 0) ? <TextNode>this.selectRandom(textNodes) : new TextNode(this.name);
        return node;
    }
    generateText(param?: any, recursiveDepth: number = 1, recursiveLimit: number = 1): string {

        let selection: Node;
        if (recursiveDepth >= recursiveLimit) {
            selection = this.selectText();
        } else {
            selection = this.selectRandom();
        }
        if (param) {
            if (Object.keys(this.optionalParameters).length > 0) {
                let resultParameters = this.optionalParameters;
                for (let key of Object.keys(resultParameters)) {
                    if (selection.optionalParameters[key]) {
                        resultParameters[key] = selection.optionalParameters[key]
                    } else {
                        resultParameters[key] = null;
                    }
                }
                param[this.name] = resultParameters;
            }
        }
        return selection.generateText(null, ++recursiveDepth, recursiveLimit);
    }
    addItem(item: string, capitalize?: boolean) {
        this.list.push(new TextNode((capitalize) ? item : item.toLowerCase()));
    }
    removeItem(item: string) {
        let i;
        this.list.find((value, index) => {
            let result = value.name == item || value.name == item.toLowerCase();
            if (result) i = index;
            return result;
        });
        if (i >= 0) {
            this.list.splice(i, 1);
        }
    }
}
import { ObjectID } from "mongodb";

export abstract class Node {

    _id: ObjectID;
    optionalParameters: any = {};

    constructor(public name: string, public position: number = 0) {
        this._id = new ObjectID();
    }

    abstract generateText(param?: any, recursiveDepth?: number, recursiveLimit?: number): string;

    addOptionalParameters(param: any) {
       this.optionalParameters = Object.assign(this.optionalParameters, param);
        console.log(this.optionalParameters);
    }

    removeOptionalParameter(param: string){
        delete this.optionalParameters[param];
    }

    getOptionalParameter(param: string){
        return this.optionalParameters[param];
    }
}
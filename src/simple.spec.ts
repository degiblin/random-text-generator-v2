import { } from 'jasmine';
import { Node } from "./node";
import { SimpleNode } from "./simple";
import { TextNode } from "./text";

describe("A SimpleNode", () => {

    let sut;
    let values;

    beforeEach(() => {
        console.log("\n[SimpleNode]")
        sut = new SimpleNode("Test");
        values = ["testa", "testb", "testc"];
        values.forEach(value => sut.addItem(value));
    })
    it("has a name", () => {
        expect(sut.name).toBe("Test");
    })
    it("adds string to list as TextNode", () => {
        sut.list = [];
        sut.addItem("TestItem");
        expect(sut.list[0].name).toEqual("TestItem".toLowerCase());
    })
    it("adds string to list as TextNode and preserves capitalization", () => {
        sut.list = [];
        sut.addItem("TestItem", true);
        expect(sut.list[0].name).toEqual("TestItem");
    })
    it("deletes string from list", () => {
        sut.list = [];
        sut.addItem("TestItem");
        expect(sut.list[0].name).toEqual("TestItem".toLowerCase());
        sut.removeItem("TestItem");
        expect(sut.list.length).toEqual(0);
    })
    it("deletes case sensitive string from list", () => {
        sut.list = [];
        sut.addItem("TestItem", true);
        expect(sut.list[0].name).toEqual("TestItem");
        sut.removeItem("TestItem");
        expect(sut.list.length).toEqual(0);
    })
    it("generateText() returns string from list", () => {
        let result = sut.generateText(null);
        expect(values).toContain(result);
    })
    it('should accept optional parameters', () => {
        let name = "Test";
        let optionalParameters = { "test": "testValue", "test2": "testValue2" };
        sut.addOptionalParameters(optionalParameters);
        expect(sut.optionalParameters).toEqual(optionalParameters);

        let resultParameters = {};
        let result = sut.generateText(resultParameters);
        expect(values).toContain(result);
    });

    it('should return child`s optional parameters', () => {
        let childNode = new TextNode('TestNode');
        childNode.addOptionalParameters({ "RequiredParameter": "Test" });
        sut.addOptionalParameters({ "RequiredParameter": true, "NullRequiredParameter": true });
        sut.list = [childNode];
        let resultParameters = {}
        let result = sut.generateText(resultParameters);
        expect(resultParameters[sut.name]['RequiredParameter']).toBe("Test");
        expect(resultParameters[sut.name]["NullRequiredParameter"]).toBeDefined();
        expect(resultParameters[sut.name]["NullRequiredParameter"]).toBeNull();
    });

    it('selectRandom() should return a Node from this.list', () => {
        let result = sut.selectRandom();
        expect(sut.list).toContain(result);
    });
    it('selectRandom() should return a Node from optional parameter', () => {
        let list = [new TextNode("Test1"), new TextNode("Test2"), new TextNode("Test3")];
        let result = sut.selectRandom(list);
        expect(list).toContain(result);
    });
    it('selectText() should return a TextNode from list', () => {
        let result = sut.selectText();
        expect(sut.list).toContain(result);
        expect(result instanceof TextNode).toBeTruthy();
    })
    it('selectText() should return a new TextNode with this.name if none in list', () => {
        sut.list = [];
        let result = sut.selectText();
        expect(result instanceof TextNode).toBeTruthy();
        expect(result.name).toBe(sut.name);
    });
    it('should call selectText() if recursiveDepth >= recursiveLimit', () => {
        spyOn(sut, "selectText").and.callThrough();
        sut.generateText(null, 1, 1);
        expect(sut.selectText).toHaveBeenCalledTimes(1);
        sut.generateText(null, 2, 1);
        expect(sut.selectText).toHaveBeenCalledTimes(2);
        sut.generateText(null, 2, 3);
        expect(sut.selectText).toHaveBeenCalledTimes(2);
    })
    class TempNode extends Node {
        list: Node[] = [];
        depth: number;
        limit: number;
        generateText(param?: any, recursiveDepth: number = 1, recursiveLimit: number = 1): string {
            this.depth = recursiveDepth;
            this.limit = recursiveLimit;
            let result = "";
            for (let i = 0; i < this.list.length; i++) {
                result += this.list[i].generateText();
            }
            return result;
        }
    }

    it('should increment recursiveDepth before passing it on to child nodes', () => {
        let result = new TempNode("Temp");
        sut.list = [result];
        sut.generateText(null, 3, 5);
        expect(result.limit).toBe(5);
        expect(result.depth).toBe(4);
    });

})
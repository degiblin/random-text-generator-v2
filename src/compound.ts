import { Node } from "./node";
import { TextNode } from "./text";

export class CompoundNode extends Node {

    parameters = {};

    generateText(param?: any, recursiveDepth: number = 1, recursiveLimit: number = 1): string {
        let results: string[] = [];
        for (let v in this.parameters) {
            if (this.parameters[v] instanceof Node) {
                /** Insert the Node in its designated index if unoccupied otherwise the next available index */
                let i = this.parameters[v].position;
                while (results[i] !== undefined) {
                    i++;
                }
                results[i] = this.parameters[v].generateText(this.optionalParameters, ++recursiveDepth, recursiveLimit);
            }
            else {
                let i = this.parameters[v].position;
                while (results[i] !== undefined) {
                    i++;
                }
                results[i] = this.parameters[v];
            }
        }

        /** Remove undefined indexes from results */
        let i = 0;
        while (i < results.length) {
            if (results[i] === undefined) {
                results.splice(i, 1)
            }
            else {
                i++
            }
        }
        if (param && Object.keys(this.optionalParameters).length > 0) {
            console.log(param);
            param[this.name] = this.optionalParameters;
        }
        return results.join(" ");
    }

    createInstance(name: string): CompoundNode {
        let result = new CompoundNode(name);
        for (let p in this.parameters) {
            if (this.parameters[p] instanceof Node) {
                result.parameters[p] = this.parameters[p].generateText(result.optionalParameters);
            } else {
                result.parameters[p] = this.parameters[p];
            }
        }
        return result;
    }

    addParameter(param: Node){
        this.parameters[param.name] = param;
    }

    addParameters(params: any) {
        for (let p in params) {
            if (params[p] instanceof Node) {
                this.parameters[p] = params[p];
            } else if (typeof params[p] === "string"){
                let textNode = new TextNode(params[p]);
                this.parameters[p] = textNode;
            }
        }
    }

    removeParameter(name: string){
        if(this.parameters[name]){
            delete this.parameters[name];
        }
    }


}
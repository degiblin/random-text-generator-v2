import { } from 'jasmine';
import { Node } from "./node";

describe("A Node", () => {

    class SUTNode extends Node {
        generateText(param?: any, recursiveDepth?: number, recursiveLimit?: number): string {
            return this.name;
        }
    }

    let sut = new SUTNode("Sut");

    beforeEach(() => {
        console.log("\n[Node]");
    });

    it('should have generated _id in constructor', () => {
        expect(sut._id).toBeTruthy();
    });

    it('should have assigned name property in constructor', () => {
        expect(sut.name).toBe("Sut");
    });

    it("should initialize optionalParameters", () => {
        expect(sut.optionalParameters).toBeDefined();
    });

    it("addOptionalParameters() should merge object into optionalParameters", () => {
        let defaults = { "Exists": "Should Still Exist" };
        sut.optionalParameters = defaults;
        let options = { "TestA": "TestValueA", "TestB": "TestValueB" };
        sut.addOptionalParameters(options);
        expect(sut.optionalParameters["TestA"]).toBe(options["TestA"]);
        expect(sut.optionalParameters["TestB"]).toBe(options["TestB"]);
        expect(sut.optionalParameters["Exists"]).toBe(defaults["Exists"]);
    });
    it("removeOptionalParameter() should delete the given parameter", () => {
        let options = { "TestA": "TestValueA", "TestB": "TestValueB" };
        sut.addOptionalParameters(options);
        expect(sut.optionalParameters["TestA"]).toBe(options["TestA"]);
        expect(sut.optionalParameters["TestB"]).toBe(options["TestB"]);
        sut.removeOptionalParameter("TestA");
        expect(Object.keys(sut.optionalParameters)).not.toContain("TestA");
        expect(sut.optionalParameters["TestB"]).toBe(options["TestB"]);
    });
    it("getOptionalParameter() should return the parameter", () => {
        let options = { "TestA": "TestValueA", "TestB": "TestValueB" };
        sut.addOptionalParameters(options);
        expect(sut.optionalParameters["TestA"]).toBe(options["TestA"]);
        expect(sut.optionalParameters["TestB"]).toBe(options["TestB"]);
        let result = sut.getOptionalParameter("TestA");
        expect(result).toBe(options["TestA"]);
    })

})
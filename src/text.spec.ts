import { } from "jasmine";
import { TextNode } from "./text"

describe("A TextNode", () => {
    beforeEach(() => { 
        console.log("\n[TextNode]")
    })
    it("should return its name in generateText()", () => {
        let name = "TestNode";
        let sut = new TextNode(name);
        let result = sut.generateText();
        expect(result).toBe(name);
    })

});